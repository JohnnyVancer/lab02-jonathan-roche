﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreatePrefab : MonoBehaviour
{

    [MenuItem("Project Tools/Create Prefab %q")]
    public static void MakePrefab()
    {
        GameObject[] selectedObjects = Selection.gameObjects;

        int i = 0;
        foreach (GameObject go in selectedObjects)
        {
            string name = go.name;
            string assetPath = "Assets/" + name + ".prefab";

            if (AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)))
            {
                Debug.Log("Asset exists!");
                if (EditorUtility.DisplayDialog("Caution", "Prefab {0} already exists. Do you want to overwrite?", "Yes", "No"))
                {
                    CreateNew(selectedObjects[i], assetPath);
                }
            }
            else
            {
                CreateNew(selectedObjects[i], assetPath);
            }
            //Debug.Log("Name: " + go.name + " Path: " + assetPath);
            i++;
        }
    }
    public static void CreateNew(GameObject obj, string location)
    {
        Object prefab = PrefabUtility.CreateEmptyPrefab(location);
        PrefabUtility.ReplacePrefab(obj, prefab);
        AssetDatabase.Refresh();

        DestroyImmediate(obj);
        GameObject clone = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
    }
}